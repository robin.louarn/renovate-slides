# 🤖 Renovate

![Qrcode pour accéder aux slides](qrcode.svg) <!-- .element: style="width: 300px; height: 300px;" -->

---

## Qu'est-ce que Renovate ?

Renovate est un outil multi-plateforme et multi-langage pour automatiser les mises à jour des dépendances. <!-- .element: class="fragment" data-fragment-index="1" -->

---

## Pourquoi utiliser Renovate ?

--

### Mises à jour automatiques

Recevez des merge requests (MR) pour mettre à jour vos dépendances et les fichiers de verrouillage.

--

### Selon votre emploi du temps

Réduisez le bruit en planifiant le moment où Renovate crée des MR.

--

### Fonctionne immédiatement

Renovate détecte automatiquement les fichiers pertinents, y compris dans les monorepos.

--

### Personnalisation

Vous pouvez personnaliser le comportement du bot avec des [fichiers de configuration](https://docs.renovatebot.com/configuration-options/):

- renovate.json

--

### Configuration prête à l'emploi

[Configuration prête à l'emploi](https://docs.renovatebot.com/config-presets/) avec des presets de type ESLint.

--

### Open source

Renovate est sous licence [GNU Affero General Public License](https://github.com/renovatebot/renovate/blob/main/license).

---

## Il existe d'autre solutions ?

Dependabot <!-- .element: class="fragment" data-fragment-index="1" -->

--

### Pourquoi Renovate plutôt que Dependabot ?

Renovate

- propose l'autodiscover <!-- .element: class="fragment" data-fragment-index="1" -->
- propose les presets (configuration partagée) <!-- .element: class="fragment" data-fragment-index="2" -->
- est compatible gitlab nativement <!-- .element: class="fragment" data-fragment-index="3" -->

--

### Pourquoi Renovate plutôt que Dependabot ? (inconvénient)

Dependabot

- implémenté via le projet dependabot-gitlab<!-- .element: class="fragment" data-fragment-index="1" -->
- nécessite l'initialisation du projet dans dépendabot <!-- .element: class="fragment" data-fragment-index="2" -->
  - commit de configuration dans le projet <!-- .element: class="fragment" data-fragment-index="3" -->
  - appel de l'api dependabot pour enregistrer le projet et sa configuration <!-- .element: class="fragment" data-fragment-index="4" -->
- ⛔ Lors des tests effectués, artifactory KO pour java et npm. <!-- .element: class="fragment" data-fragment-index="5" -->

---

## Façons d'exécuter Renovate

Vous pouvez exécuter Renovate en tant que :

- [Package npm](https://www.npmjs.com/package/renovate)
- [Image Docker](https://hub.docker.com/r/renovate/renovate)

Ou vous pouvez utiliser l'application Mend Renovate hébergée par [Mend](https://www.mend.io/).

--

### Premier pas en local

```sh
npx renovate
  --platform gitlab
  --endpoint $RENOVATE_TOKEN
  --endpoint $CI_API_V4_URL
  groupe1/repo1
```

--

### Premier pas dans la CI

Intégration dans gitlab CI : [renovate-bot/renovate-runner](https://gitlab.com/renovate-bot/renovate-runner/-/blob/main/templates/renovate.gitlab-ci.yml#L1-19)

--

### Mise au propre

config.js

```js [5-16|9-15|12-13|17]
/**
 * @type {import('renovate/dist/config/types').RenovateConfig}
 */
module.exports = {
  platform: 'gitlab',
  endpoint: process.env.CI_API_V4_URL,
  repositoryCache: 'enabled',
  logFile: `${process.env.CI_PROJECT_DIR}/renovate-log.ndjson`,
  onboardingConfig: {
    $schema: 'https://docs.renovatebot.com/renovate-schema.json',
    extends: [
      'local>centre_de_competences_web_et_mobile/9058/discovery/sofa-renovate//presets/sofa',
      'local>centre_de_competences_web_et_mobile/9058/discovery/sofa-renovate//presets/gitmoji',
    ],
  },
  baseDir: `${process.env.CI_PROJECT_DIR}/renovate`,
  allowedPostUpgradeCommands: ['^npx sofa-app .*$'],
}
```

---

## 🔑 Concepts clés

--

### Comment fonctionne Renovate

Renovate effectue généralement ces étapes :

- Clonage du projet <!-- .element: class="fragment" data-fragment-index="1" -->
- Analyse des fichiers de configurations pour extraire les dépendances <!-- .element: class="fragment" data-fragment-index="2" -->
- Recherche dans les registres (Artifactory) pour vérifier les mises à jour <!-- .element: class="fragment" data-fragment-index="3" -->
- Application des règles de regroupement définies <!-- .element: class="fragment" data-fragment-index="4" -->
- Push des branches et création des MR <!-- .element: class="fragment" data-fragment-index="5" -->

--

### Onboarding

Une fois que vous avez activé le bot, vous obtiendrez une MR "Configure Renovate"

--

### Tableau de bord des dépendances

Lorsque le tableau de bord des dépendances est activé, Renovate crée une nouvelle issue. Cette issue dispose d'un « tableau de bord » où vous pouvez obtenir un aperçu de l'état de toutes les mises à jour.

--

### Les presets

Utilisez les presets pour :

- Configurer le bot avec de bons paramètres par défaut <!-- .element: class="fragment" data-fragment-index="1" -->
- Réduire la duplication de votre configuration <!-- .element: class="fragment" data-fragment-index="2" -->
- Partager votre configuration avec d'autres <!-- .element: class="fragment" data-fragment-index="3" -->
- Utiliser la configuration de quelqu'un d'autre et l'étendre avec vos propres règles <!-- .element: class="fragment" data-fragment-index="4" -->

---

## 🚀 Démo Sofa

[sofa-realworld-app](https://gitlark.s.arkea.com/centre_de_competences_web_et_mobile/9058/sofa-realworld-app)

---

## Architecture cible

![archi renovate](img/renovate.drawio.png)

--

### crontab renovate

Déployée par argocd sur la plateforme kapla on-premise

--

### Dépot git presets

Contient les configurations prédéfinies pour les différents socles

--

### Applications ciblées

Les applications ayant le topic renovate-enabled de positionné seront recherchés par renovate

---

## 🚀 Démo PaaS

[ci-tests](https://gitlark.s.arkea.com/paas/gt01/ci-tests)

---

## Merci ! 🙏

![Qrcode pour accéder aux slides](qrcode.svg) <!-- .element: style="width: 300px; height: 300px;" -->
