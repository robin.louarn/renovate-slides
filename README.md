# renovate-slide

## Usage

```sh
# install
pnpm install

# start dev server
pnpm dev
```

## Speaker mode

Press "S" for enable speaker mode.

## Notes

For enable the notes in dev mode, create `.env` :

```sh
VITE_SHOW_NOTES="true"
```
