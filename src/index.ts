import Reveal from 'reveal.js'
import Notes from 'reveal.js/plugin/notes/notes.esm'
import Markdown from 'reveal.js/plugin/markdown/markdown.esm'
import Highlight from 'reveal.js/plugin/highlight/highlight.esm'

import 'reveal.js/dist/reveal.css'
import 'reveal.js/dist/theme/black.css'
import 'reveal.js/plugin/highlight/monokai.css'

const deck = new Reveal()

deck.initialize({
  hash: true,
  slideNumber: true,
  showNotes: import.meta.env.VITE_SHOW_NOTES === 'true',
  plugins: [Notes, Markdown, Highlight],
})
